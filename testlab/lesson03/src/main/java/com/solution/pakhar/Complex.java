package com.solution.pakhar;

/**
 * Complex
 *
 * @author Bohdan Pakhar
 * @version 1.1 21.07.2022
 */
public class Complex {

    private final double RE;
    private final double IM;

    /**
     * initializes values RE and IM if they aren't Nan, else throw exception
     *
     * @param IM imaginary part
     * @param RE real part
     */
    public Complex(double RE, double IM) {
        if (Double.isNaN(RE) || Double.isNaN(IM)) {
            throw new ArithmeticException();
        }
        this.RE = RE;
        this.IM = IM;
    }

    public static void main(String[] args) {
        Complex z = new Complex(1, 2);
        System.out.println(z.add(z));
    }

    public double realPart() {
        return RE;
    }

    public double imaginaryPart() {
        return IM;
    }

    /**
     * create a new object with added values
     *
     * @param c is object of "Complex" class
     * @return object Complex
     */
    public Complex add(Complex c) {
        return new Complex(RE + c.RE, IM + c.IM);
    }

    /**
     * compares both objects
     *
     * @param o object for comparing
     * @return {@code true} if object is itself or RE, IM values
     * of objects are equal, and
     * {@code false} if object isn't "Complex" type or RE, IM
     * values of objects are not equal
     */
    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Complex c))
            return false;
        return Double.compare(RE, c.RE) == 0 &&
                Double.compare(IM, c.IM) == 0;
    }

    /**
     * generate hash code for values IM, RE
     *
     * @return {@code int} values
     */
    @Override
    public int hashCode() {
        int result = 17 + Double.hashCode(RE);
        result = 31 * result + Double.hashCode(IM);
        return result;
    }

    /**
     * string formation
     *
     * @return {@code String}
     */
    @Override
    public String toString() {
        return "(" + RE + (IM < 0 ? "" : "+") + IM + "i)";
    }
}
